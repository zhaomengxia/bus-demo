package com.springcloudbus.bussender.controller;

import com.springcloudbus.bussender.config.Sender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author zhaomengxia
 * @create 2019/9/4 17:03
 */
@RestController
public class BusSenderController {

    @Autowired
    private Sender sender;

    @RequestMapping("/send")
    public String send(){
        for (int i = 0; i < 1000; i++) {
            sender.send(i);
        }
        return "success";
    }
}
