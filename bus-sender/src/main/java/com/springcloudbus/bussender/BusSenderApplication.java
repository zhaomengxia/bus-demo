package com.springcloudbus.bussender;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BusSenderApplication {

	public static void main(String[] args) {
		SpringApplication.run(BusSenderApplication.class, args);
	}

}
