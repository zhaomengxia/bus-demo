package com.springcloudbus.busreceiver2;

import org.springframework.amqp.core.Queue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class BusReceiver2Application {

	public static void main(String[] args) {
		SpringApplication.run(BusReceiver2Application.class, args);
	}


	@Bean
	public Queue  helloQueue(){
		return new Queue("hello2");
	}
}
