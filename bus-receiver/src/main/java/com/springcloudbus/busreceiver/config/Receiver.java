package com.springcloudbus.busreceiver.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author zhaomengxia
 * @create 2019/9/4 17:09
 */
@Component
@RabbitListener(queues = "hello")
public class Receiver {
    private static final Logger log= LoggerFactory.getLogger(Receiver.class);

    @RabbitHandler
    public void process(String hello){
        log.info("Receiver info :{}",hello);
    }
}
