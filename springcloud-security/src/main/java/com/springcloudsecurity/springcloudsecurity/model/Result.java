package com.springcloudsecurity.springcloudsecurity.model;

import lombok.Data;

/**
 * @Author zhaomengxia
 * @create 2019/9/12 11:52
 */
@Data
public class Result {
    private int code;
    private String message;
    private Object data;
}
