package com.springcloudsecurity.springcloudsecurity.controller;

import com.springcloudsecurity.springcloudsecurity.enums.ResultCode;
import com.springcloudsecurity.springcloudsecurity.model.Result;
import com.springcloudsecurity.springcloudsecurity.service.CustomUserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.token.ConsumerTokenServices;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

/**
 * @Author zhaomengxia
 * @create 2019/9/12 11:41
 */
@RestController
@RequestMapping("/api/")
public class UserController {

    @Autowired
    private CustomUserDetailService userDetailService;
    @Autowired
    private ConsumerTokenServices consumerTokenServices;


    @GetMapping("/user")
    public Principal user(Principal user){
        //获取当前用户信息
        return user;
    }
    @DeleteMapping(value = "/exit")
    public Result revokeToken(String access_token) {
        //注销当前用户
        Result result = new Result();
        if (consumerTokenServices.revokeToken(access_token)) {
            result.setCode(ResultCode.SUCCESS.getCode());
            result.setMessage("注销成功");
        } else {
            result.setCode(ResultCode.FAILED.getCode());
            result.setMessage("注销失败");
        }
        return result;
    }

}
