package com.springcloudsecurity.springcloudsecurity;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@SpringBootApplication
//@EnableResourceServer//开启安全监控
@EnableDiscoveryClient
public class SpringcloudSecurityApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringcloudSecurityApplication.class, args);
	}

}
