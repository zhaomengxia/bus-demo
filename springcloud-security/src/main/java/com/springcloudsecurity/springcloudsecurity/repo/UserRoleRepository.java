package com.springcloudsecurity.springcloudsecurity.repo;

import com.springcloudsecurity.springcloudsecurity.model.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author zhaomengxia
 * @create 2019/9/12 13:34
 */
@Repository
public interface UserRoleRepository extends JpaRepository<UserRole,Long> {
    List<UserRole> findByUserId(long userId);
}
