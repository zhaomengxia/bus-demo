package com.springcloudsecurity.springcloudsecurity.repo;

import com.springcloudsecurity.springcloudsecurity.model.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author zhaomengxia
 * @create 2019/9/12 13:42
 */
@Repository
public interface MenuRepository extends JpaRepository<Menu,Long> {
    List<Menu> findByMenuIdIn(List<Long> menuIds);

}
