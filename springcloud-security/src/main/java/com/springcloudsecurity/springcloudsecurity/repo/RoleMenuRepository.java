package com.springcloudsecurity.springcloudsecurity.repo;

import com.springcloudsecurity.springcloudsecurity.model.RoleMenu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author zhaomengxia
 * @create 2019/9/12 13:40
 */
@Repository
public interface RoleMenuRepository extends JpaRepository<RoleMenu,Long> {
    List<RoleMenu> findByRoleIdIn(List<Long> roleIds);
}
