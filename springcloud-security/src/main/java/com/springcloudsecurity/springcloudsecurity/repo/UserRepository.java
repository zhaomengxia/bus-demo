package com.springcloudsecurity.springcloudsecurity.repo;

import com.springcloudsecurity.springcloudsecurity.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * @Author zhaomengxia
 * @create 2019/9/12 11:57
 */
@Repository
public interface UserRepository extends JpaRepository<User,Long> {

    User findByUsername(String userName);
}
