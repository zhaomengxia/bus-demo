package com.springcloudsecurity.springcloudsecurity.repo;

import com.springcloudsecurity.springcloudsecurity.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @Author zhaomengxia
 * @create 2019/9/12 13:35
 */
@Repository
public interface RoleRepository extends JpaRepository<Role,Long> {

    List<Role> findByRoleIdIn(List<Long> roleIds);
}
