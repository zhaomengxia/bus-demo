//package com.springcloudsecurity.springcloudsecurity.config;
//
//import com.springcloudsecurity.springcloudsecurity.service.CustomUserDetailService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.autoconfigure.security.SecurityProperties;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.core.annotation.Order;
//import org.springframework.security.authentication.AuthenticationManager;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
//import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
//import org.springframework.security.config.annotation.web.builders.HttpSecurity;
//import org.springframework.security.config.annotation.web.builders.WebSecurity;
//import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
//
///**
// * 实现用户认证和授权管理
// * 主要继承WebSecurityConfigurerAdapter实现访问资源拦截配置。该拦截器的顺序在资源服务器拦截器之前
// * @Author zhaomengxia
// * @create 2019/9/6 14:52
// */
//@Configuration
//@Order(SecurityProperties.BASIC_AUTH_ORDER)
//public class OAuthWebSecurityConfigurer extends WebSecurityConfigurerAdapter {
//
//
//    @Autowired
//    private CustomUserDetailService customUserDetailService;
//
//    @Override
//    @Bean
//    public AuthenticationManager authenticationManagerBean() throws Exception {
//        return super.authenticationManagerBean();
//    }
//
//    @Override
//    @Bean
//    public UserDetailsService userDetailsServiceBean() throws Exception {
//        return super.userDetailsServiceBean();
//    }
//
//    @Bean
//    public static BCryptPasswordEncoder passwordEncoder(){
//        return new BCryptPasswordEncoder();
//    }
//
//    @Override
//    protected void configure(AuthenticationManagerBuilder auth) throws Exception {
//        auth.inMemoryAuthentication()
//                .withUser("admin")
//                .password("123456")
//                .roles("USER", "ADMIN")
//                .and()
//                .withUser("user")
//                .password("123456")
//                .roles("USER");
//    }
//
//    @Override
//    protected void configure(HttpSecurity http) throws Exception {
//        //关闭csrf
//        http.csrf().disable();
//        //拦截所有请求
//        http.authorizeRequests()
//                .antMatchers("/oauth/remove_token")
//                .permitAll()
//                .anyRequest()
//                .authenticated();
//    }
//
//    @Override
//    public void configure(WebSecurity web) throws Exception {
//        web.ignoring().antMatchers("/favor.ico");
//    }
//
//    @Bean
//    public AuthenticationProvider authenticationProvider() {
//
//        DaoAuthenticationProvider authenticationProvider=new DaoAuthenticationProvider();
//
//        authenticationProvider.setUserDetailsService(customUserDetailService);
//        authenticationProvider.setPasswordEncoder(passwordEncoder());
//        authenticationProvider.setHideUserNotFoundExceptions(false);
//        return authenticationProvider;
//    }
//}
