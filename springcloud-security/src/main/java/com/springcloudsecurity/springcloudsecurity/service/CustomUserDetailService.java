package com.springcloudsecurity.springcloudsecurity.service;

import com.springcloudsecurity.springcloudsecurity.model.*;
import com.springcloudsecurity.springcloudsecurity.repo.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;


/**
 * @Author zhaomengxia
 * @create 2019/9/6 16:07
 */
@Service
public class CustomUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private RoleMenuRepository roleMenuRepository;
    @Autowired
    private MenuRepository menuRepository;
    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {

        User user=userRepository.findByUsername(s);
        List<UserRole> userRoles=userRoleRepository.findByUserId(user.getUserId());
        List<Long> roleIds=userRoles.stream().map(a->a.getRoleId()).collect(Collectors.toList());
        List<Role> roles=roleRepository.findByRoleIdIn(roleIds);
        List<RoleMenu> roleMenus=roleMenuRepository.findByRoleIdIn(roleIds);
        Map<Long,List<RoleMenu>> longListMap=roleMenus.stream().collect(Collectors.groupingBy(a->a.getRoleId()));

        List<Long> menuIds=roleMenus.stream().map(a->a.getMenuId()).collect(Collectors.toList());
        List<Menu> menus=menuRepository.findByMenuIdIn(menuIds);

        Map<Long,Menu> longMenuMap=menus.stream().collect(Collectors.toMap(a->a.getMenuId(),a->a));
        if (user==null){
            throw new UsernameNotFoundException(s);
        }
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        // 可用性 :true:可用 false:不可用
        boolean enabled = true;
        // 过期性 :true:没过期 false:过期
        boolean accountNonExpired = true;
        // 有效性 :true:凭证有效 false:凭证无效
        boolean credentialsNonExpired = true;
        // 锁定性 :true:未锁定 false:已锁定
        boolean accountNonLocked = true;

        for (Role role:roles) {
            //角色必须是ROLE_开头，可以在数据库中设置
            GrantedAuthority grantedAuthority = new SimpleGrantedAuthority(role.getRoleName());
            grantedAuthorities.add(grantedAuthority);
            Long roleId=role.getRoleId();
            if (longListMap.containsKey(roleId)){
                List<RoleMenu> roleMenus1=longListMap.get(roleId);
                //获取权限
                for (RoleMenu roleMenu : roleMenus1) {
                    Long menuId=roleMenu.getMenuId();
                    if (longMenuMap.containsKey(menuId)){
                        Menu menu=longMenuMap.get(menuId);
                        GrantedAuthority authority = new SimpleGrantedAuthority(menu.getUrl());
                        grantedAuthorities.add(authority);
                    }
                }

            }
        }

        org.springframework.security.core.userdetails.User user1=
                new org.springframework.security.core.userdetails.User(user.getUsername(),user.getPassword(),enabled,
                        accountNonExpired,credentialsNonExpired,accountNonLocked,grantedAuthorities);

        return user1;
    }
}
